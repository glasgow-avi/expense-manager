package com.catch22.expensemanager.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ActivityFactory
{
    public static void navigate(final Activity source, final Class<?> destination)
    {
        Intent nextIntent = new Intent(source, destination);
        source.startActivity(nextIntent);
        source.finish();
    }

    public static void setNavigation(View button, final AppCompatActivity source, final Class<?> destination)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                navigate(source, destination);
            }
        });
    }
}

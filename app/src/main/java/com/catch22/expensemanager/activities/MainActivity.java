/***
 * TODO: Editing expenses
 * View profile
 * Picture of the bill
 */

package com.catch22.expensemanager.activities;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.catch22.expensemanager.R;
import com.catch22.expensemanager.db.DatabaseHandler;
import com.catch22.expensemanager.db.DatabaseMaster;
import com.catch22.expensemanager.gui.DataProvider;
import com.catch22.expensemanager.gui.ExpandableListViewAdapter;
import com.catch22.expensemanager.notification.AlarmReceiver;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    //LIST VIEW
    private static ExpandableListView listView; //the expandable list view
    private static ExpandableListViewAdapter listAdapter; //the expandable list view adapter
    private static HashMap<String, List<String>> parentNameToParentListMap; //maps the name of the expense class, to the actual expense class
    private static List<String> parentList; //list of all names of the expense classes

    //NOTIFICATIONS
    private PendingIntent pendingIntent;

    //DATA VARIABLES
//    public static Expense currentlySelectedExpense = null; //the expense that has been long pressed
    public static String userName;
    private static Toast toast = null; //popup message which shows the information of each expense, on click

    //ADVERTISEMENTS
    private static AdView adView;

    //DATABASE
    public static DatabaseMaster databaseMaster;

    //POPUPS
    private static Button contextMenuCreatorButton; //an invisible button, used for click simulation to create the context menu

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SettingsActivity.loadSettings(this);

        databaseMaster = new DatabaseMaster(this);
        ExpenseAdder.databaseMaster = databaseMaster;

        databaseMaster.deleteAllEntries();
        DatabaseHandler.addExpense("new expense", 60, "No Information");

        adView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("372B169E7D0E33EDFE44CEDDFEC91366").build();

        adView.loadAd(adRequest);

        scheduleNotification();

        updateNames(databaseMaster.getWritableDatabase());
        ExpenseAdder.editing = false;

        //FINDING ALL VIEWS BY ID
        listView = (ExpandableListView) findViewById(R.id.listView);
        contextMenuCreatorButton = (Button) findViewById(R.id.button_context_menu_creator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" " + ("".equals(userName) ? "" : "Hi, " + userName + " :)"));

        FloatingActionButton addExpenseButton = (FloatingActionButton) findViewById(R.id.button_add_expense_activity);
        addExpenseButton.setImageResource(R.drawable.add_expense_icon);

        registerForContextMenu(contextMenuCreatorButton);
        ActivityFactory.setNavigation(addExpenseButton, MainActivity.this, ExpenseAdder.class);

        parentNameToParentListMap = DataProvider.getInfo();
        parentList = new ArrayList<>(parentNameToParentListMap.keySet());
        listAdapter = new ExpandableListViewAdapter(this, parentNameToParentListMap, parentList);
        listView.setAdapter(listAdapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD)
                {
                    int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    int childPosition = ExpandableListView.getPackedPositionChild(id);

//                    currentlySelectedExpense = ExpenseManager.getExpense(parentList.get(groupPosition).split(" → ")[0], childPosition + 1);

                    contextMenuCreatorButton.performLongClick();

                    return true;
                }

                return false;
            }
        });

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener()
        {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
            {
                String clickedParent = parentList.get(groupPosition);
                Log.d("On Child Click", clickedParent);
                List<String> clickedList = parentNameToParentListMap.get(clickedParent);
                Log.d("On Child Click", clickedList.get(0));
                String clickedChild = clickedList.get(childPosition);
                Log.d("On Child Click", clickedChild);
                String[] clickedChildParts = clickedChild.split("#");
                int expenseId = Integer.valueOf(clickedChildParts[clickedChildParts.length - 1]);
                Log.d("On Child Click", String.valueOf(expenseId));
                String toastMessage = DatabaseHandler.getInformation(expenseId);
                if(toast != null)
                    toast.cancel();
                if ("".equals(toastMessage)) //there is no information stored about this object
                    return false;
                toast = Toast.makeText(MainActivity.this, toastMessage, Toast.LENGTH_SHORT);
                toast.show();
                return false;
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void scheduleNotification()
    {
        Intent myIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 22);
        calendar.set(Calendar.HOUR_OF_DAY, 30);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 60 * 24, pendingIntent);
    }

    @Override
    protected void onStop()
    {
        if (toast != null)
            toast.cancel();

        super.onStop();
    }

    public static void updateNames(SQLiteDatabase db)
    {
        ExpenseAdder.names = DatabaseHandler.getAllCategories(db);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        try
        {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.context_menu, menu);
        } catch(Throwable e)
        {
            Log.d("Context Menu", "Failed to create");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.popup_menu_view:
                break;
            case R.id.popup_menu_remove:
//                ExpenseManager.removeExpense(currentlySelectedExpense.name, ExpenseManager.getIndex(currentlySelectedExpense));
                MainActivity.updateNames(databaseMaster.getWritableDatabase());
//                FileHandler.write(this);
                ActivityFactory.navigate(MainActivity.this, MainActivity.class);
                break;
            case R.id.popup_menu_edit:
                ExpenseAdder.editing = true;
                ActivityFactory.navigate(MainActivity.this, ExpenseAdder.class);
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_settings)
        {
            ActivityFactory.navigate(MainActivity.this, SettingsActivity.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
package com.catch22.expensemanager.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.catch22.expensemanager.R;
import com.catch22.expensemanager.db.DatabaseHandler;
import com.catch22.expensemanager.db.DatabaseMaster;
import com.catch22.expensemanager.files.FileHandler;

import java.io.File;
import java.io.IOException;

public class ExpenseAdder extends AppCompatActivity
{
    private ArrayAdapter nameAdapter;
    public static String[] names;

    private ImageButton billAdder;
    private EditText amountEditText, informationEditText;
    private AutoCompleteTextView nameEditText;

    public static boolean editing;

    private static final int CAMERA_REQUEST = 1002;
    public static String imageFileName;

    public static DatabaseMaster databaseMaster;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_adder);

        databaseMaster = new DatabaseMaster(this);

        FloatingActionButton addExpenseButton = (FloatingActionButton) findViewById(R.id.button_add_expense);
        addExpenseButton.setImageResource(R.drawable.add_expense_icon);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_expense_adder);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        billAdder = (ImageButton) findViewById(R.id.image_button_add_bill);
        amountEditText = (EditText) findViewById(R.id.amountEditText);
        informationEditText = (EditText) findViewById(R.id.informationEditText);
        nameEditText = (AutoCompleteTextView) findViewById(R.id.nameEditText);

        billAdder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if(cameraIntent.resolveActivity(getPackageManager()) != null)
                {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try
                    {
                        photoFile = FileHandler.createImageFile(ExpenseAdder.this);
                    } catch(IOException ex)
                    {
                        ex.printStackTrace();
                    }
                    // Continue only if the File was successfully created
                    if(photoFile != null)
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });

        if(editing)
        {
//            amountEditText.setText(String.valueOf(MainActivity.currentlySelectedExpense.value));
//            nameEditText.setText(String.valueOf(MainActivity.currentlySelectedExpense.name));
//            informationEditText.setText(String.valueOf(MainActivity.currentlySelectedExpense.information));
        }

        nameAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, names);

        nameEditText.setThreshold(1);
        nameEditText.setAdapter(nameAdapter);

        addExpenseButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    String name = nameEditText.getText().toString();
                    if("".equals(name))
                        name = "Miscellaneous";

                    int amount = Integer.valueOf(amountEditText.getText().toString());
                    String information = informationEditText.getText().toString();
                    if(editing)
                    {
//                        ExpenseManager.editExpense(MainActivity.currentlySelectedExpense.name, ExpenseManager.getIndex(MainActivity.currentlySelectedExpense), amount, name, information);
                        editing = false;
                    }
                    else
                    {
                        DatabaseHandler.addExpense(name, amount, information);
                        nameEditText.setText("");
                        amountEditText.setText("");
                        informationEditText.setText("");
                    }

                    MainActivity.updateNames(databaseMaster.getWritableDatabase());
//                    FileHandler.write(ExpenseAdder.this);

                    if(editing)
                        ActivityFactory.navigate(ExpenseAdder.this, MainActivity.class);

                    editing = false;

                    billAdder.setImageBitmap(null);

                } catch(Throwable t)
                {
                    Toast.makeText(ExpenseAdder.this, "Check your input format", Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK)
        {
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
            billAdder.setImageBitmap(imageBitmap);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
            ActivityFactory.navigate(ExpenseAdder.this, MainActivity.class);

        return super.onKeyDown(keyCode, event);
    }
}
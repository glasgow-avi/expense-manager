package com.catch22.expensemanager.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.catch22.expensemanager.R;
import com.catch22.expensemanager.activities.MainActivity;

public class AlarmReceiver extends BroadcastReceiver
{
    public static boolean notificationsOn;
    public static String notificationSoundUri;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d("Notification", "Received");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(!notificationsOn)
            return;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent repeatingIntent = new Intent(context, MainActivity.class);
        repeatingIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, repeatingIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(Uri.parse(notificationSoundUri))
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText("Hi" + ("".equals(MainActivity.userName) ? "" : ", " + MainActivity.userName) + "! Did you update your expenses today?")
                .setAutoCancel(true);

        notificationManager.notify(100, builder.build());
    }
}
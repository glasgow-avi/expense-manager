package com.catch22.expensemanager.gui;

import android.database.Cursor;

import com.catch22.expensemanager.activities.MainActivity;
import com.catch22.expensemanager.db.DatabaseHandler;
import com.catch22.expensemanager.db.DatabaseMaster;

import java.util.HashMap;
import java.util.List;

public class DataProvider
{
//    public static HashMap<String, List<String>> getInfo()
//    {
//        HashMap<String, List<String>> expenses = new HashMap<>();
//
//        //for each category
//        for(ArrayList<Expense> expenseList : ExpenseManager.expenses)
//        {
//            Log.d("Expense", expenseList.get(0).name);
//            List<String> list = new ArrayList<>(); //this category
//
//            for(int index = 1; index < expenseList.size(); index++)
//            {
//                Log.d("Expense", '\t' + expenseList.get(index).toString());
//                list.add(expenseList.get(index).toString());
//            }
//
//            expenses.put(expenseList.get(0).toString(), list);
//        }
//
//        return expenses;
//    }

    public static HashMap<String, List<String>> getInfo()
    {
        HashMap<String, List<String>> expenses = new HashMap<>();

        Cursor c = MainActivity.databaseMaster.getWritableDatabase().rawQuery("select " + DatabaseMaster.CATEGORY_ID + " from " + DatabaseMaster.CATEGORY_TABLE, null);

        while(c.moveToNext())
        {
            int catId = c.getInt(0);
            List<String> category = DatabaseHandler.getAllExpensesInCategory(catId);
            expenses.put(DatabaseHandler.getCategory(catId), category);
        }

        return expenses;
    }
}
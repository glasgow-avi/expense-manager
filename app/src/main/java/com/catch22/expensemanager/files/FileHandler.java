package com.catch22.expensemanager.files;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.catch22.expensemanager.activities.ExpenseAdder;
import com.catch22.expensemanager.db.DatabaseHandler;
import com.catch22.expensemanager.db.DatabaseMaster;

import java.io.File;
import java.io.IOException;

public class FileHandler
{
    private static final String fileName = "expenses.dat";
    private static final String imageFolderName = "bills";

//    public static void read(Context context)
//    {
//        FileInputStream inputStream;
//
//        try
//        {
//            Log.d("File", "Trying to read file");
//            inputStream = context.openFileInput(fileName);
//            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
//            ExpenseManager.expenses = (ArrayList<ArrayList<Expense>>) objectInputStream.readObject();
//            Log.d("File", "Read");
//
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    public static void remove(Context context)
//    {
//        try
//        {
//            Log.d("File", "Trying to remove file");
//            File file = new File(context.getFilesDir(), fileName);
//            file.delete();
//            Log.d("File", "Deleted");
//        } catch (Exception fileDoesNotExist)
//        {
//
//        }
//    }
//
//    public static void write(Context context)
//    {
//        FileOutputStream outputStream;
//
//        remove(context);
//
//        try
//        {
//            Log.d("File", "Trying to write file");
//            outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
//            objectOutputStream.writeObject(ExpenseManager.expenses);
//            outputStream.close();
//            Log.d("File", "Written");
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }

    public static File createImageFile(Context context) throws IOException
    {
        // Create an image file name
        String imageFileName = String.valueOf(DatabaseHandler.generateId(ExpenseAdder.databaseMaster.getWritableDatabase(), DatabaseMaster.EXPENSE_TABLE));
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        Log.d("File", image.getAbsolutePath());

        // Save a file: path for use with ACTION_VIEW intents
        ExpenseAdder.imageFileName = "file:" + image.getAbsolutePath();
        return image;
    }
}
package com.catch22.expensemanager.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.catch22.expensemanager.activities.ExpenseAdder;
import com.catch22.expensemanager.activities.MainActivity;
import com.catch22.expensemanager.activities.SettingsActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.catch22.expensemanager.db.DatabaseMaster.AttributeValuePair;
import static com.catch22.expensemanager.db.DatabaseMaster.CATEGORY_ID;
import static com.catch22.expensemanager.db.DatabaseMaster.CATEGORY_NAME;
import static com.catch22.expensemanager.db.DatabaseMaster.CATEGORY_TABLE;
import static com.catch22.expensemanager.db.DatabaseMaster.EXPENSE_AMOUNT;
import static com.catch22.expensemanager.db.DatabaseMaster.EXPENSE_CATEGORY;
import static com.catch22.expensemanager.db.DatabaseMaster.EXPENSE_DATE;
import static com.catch22.expensemanager.db.DatabaseMaster.EXPENSE_ID;
import static com.catch22.expensemanager.db.DatabaseMaster.EXPENSE_INFORMATION;
import static com.catch22.expensemanager.db.DatabaseMaster.EXPENSE_TABLE;

public class DatabaseHandler
{
    public static void addExpense(String name, int value, String information)
    {
        SQLiteDatabase db = ExpenseAdder.databaseMaster.getWritableDatabase();

        int id = generateId(db, EXPENSE_TABLE);

        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy (hh:mm)");
        String dateNow = formatter.format(currentDate.getTime());

        ExpenseAdder.databaseMaster.insertInto(EXPENSE_TABLE,
                                                new AttributeValuePair(EXPENSE_ID, id),
                                                new AttributeValuePair(EXPENSE_DATE, dateNow),
                                                new AttributeValuePair(EXPENSE_AMOUNT, value),
                                                new AttributeValuePair(EXPENSE_CATEGORY, getCategoryId(db, name)),
                                                new AttributeValuePair(EXPENSE_INFORMATION, information));
    }

    private static int getCategoryId(SQLiteDatabase db, String name)
    {
        Cursor c = db.rawQuery("select " + CATEGORY_ID +
                                " from " + CATEGORY_TABLE +
                                " where " + CATEGORY_NAME + " = '" + name +
                                "'", null);

        if(c.getCount() == 0)
        {
            int id = generateId(db, CATEGORY_TABLE);
            ExpenseAdder.databaseMaster.insertInto(CATEGORY_TABLE,
                                                    new AttributeValuePair(CATEGORY_ID, id),
                                                    new AttributeValuePair(CATEGORY_NAME, name));
            return id;
        }

        c.moveToNext();
        return c.getInt(0);
    }

    public static int generateId(SQLiteDatabase db, String tableName)
    {
        Cursor c = db.rawQuery("select max(id) from " + tableName, null);

        if(c.getCount() == 0)
            return 1;

        c.moveToNext();
        return c.getInt(0) + 1;
    }

    public static String[] getAllCategories(SQLiteDatabase db)
    {
        db.execSQL("drop view if exists x");
        db.execSQL("create view x as select A." + EXPENSE_AMOUNT +
                    ", B." + CATEGORY_NAME +
                    " from " + EXPENSE_TABLE + " A, " +
                    CATEGORY_TABLE + " B " +
                    " where " + "A." + EXPENSE_CATEGORY + " = B." + CATEGORY_ID);

        Cursor c = db.rawQuery("select " + CATEGORY_NAME + ", sum(" + EXPENSE_AMOUNT + ")" +
                                " from x" +
                                " group by " + CATEGORY_NAME
                                , null);

//        Cursor c = db.rawQuery("select " + CATEGORY_NAME +
//                        " from " + CATEGORY_TABLE
//                , null);

        ArrayList<String> categories = new ArrayList<>();
        while(c.moveToNext())
        {
            Log.d("List", c.getString(0) + " " + SettingsActivity.arrow + " " + c.getString(1));
            categories.add(c.getString(0) + SettingsActivity.arrow + SettingsActivity.currency + c.getString(1));
        }

        String[] result = new String[categories.size()];
        for(int index = 0; index < result.length; index++)
            result[index] = categories.get(index);

        db.execSQL("drop view x");

        return result;
    }

    public static String getCategory(int categoryId)
    {
        SQLiteDatabase db = MainActivity.databaseMaster.getWritableDatabase();
        Cursor c = db.rawQuery("select " + CATEGORY_NAME +
                                " from " + CATEGORY_TABLE +
                                " where " + CATEGORY_ID + " = " + categoryId,
                                null);

        c.moveToNext();

        return c.getString(0);
    }

    public static List<String> getAllExpensesInCategory(int categoryId)
    {
        SQLiteDatabase db = MainActivity.databaseMaster.getWritableDatabase();
        Cursor c = db.rawQuery("select " + EXPENSE_AMOUNT + ", " + EXPENSE_DATE + ", " + EXPENSE_ID +
                                " from " + EXPENSE_TABLE +
                                " where " + EXPENSE_CATEGORY + " = " + categoryId,
                                null);

        List<String> listOfExpenses = new ArrayList<>();
        while(c.moveToNext())
            listOfExpenses.add(SettingsActivity.currency +
                                c.getString(0) +
                                " spent on " +
                                c.getString(1) +
                                " #" + c.getString(2));

        Log.d("Database Handler", "hello");

        return listOfExpenses;
    }

    public static String getInformation(int expenseId)
    {
        SQLiteDatabase db = MainActivity.databaseMaster.getWritableDatabase();
        Cursor c = db.rawQuery("select " + EXPENSE_INFORMATION +
                                " from " + EXPENSE_TABLE +
                                " where " + EXPENSE_ID + " = " + expenseId,
                                null);

        c.moveToNext();
        return c.getString(0);
    }
}
package com.catch22.expensemanager.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseMaster extends SQLiteOpenHelper
{
    protected static class Column
    {
        public String name;
        public String dataType;
        public String constraints;

        public Column(String name, String dataType)
        {
            this.name = name;
            this.dataType = dataType;
            this.constraints = "";
        }

        public Column(String name, String dataType, String constraints)
        {
            this.name = name;
            this.dataType = dataType;
            this.constraints = constraints;
        }
    }

    protected static class AttributeValuePair
    {
        public String attribute;
        public Object value;

        public AttributeValuePair(String attribute, Object value)
        {
            this.attribute = attribute;
            this.value = value;
        }
    }

    public static final String DATABASE_NAME = "Expense_Manager.db";
    public static String EXPENSE_TABLE = "Expenses",
            EXPENSE_ID = "Id", EXPENSE_AMOUNT = "Amount", EXPENSE_DATE = "Expense_Date", EXPENSE_INFORMATION = "Information", EXPENSE_CATEGORY = "Name";
    public static String CATEGORY_TABLE = "Categories",
            CATEGORY_ID = "Id", CATEGORY_NAME = "Category";

    public DatabaseMaster(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("pragma foreign_keys = on");

        onUpgrade(db, 1, 1);

        createTable(db, CATEGORY_TABLE,
                new Column(CATEGORY_ID, "integer", "primary key"),
                new Column(CATEGORY_NAME, "text"));

        createTable(db, EXPENSE_TABLE,
                new Column(EXPENSE_ID, "integer", "primary key"),
                new Column(EXPENSE_AMOUNT, "int"),
                new Column(EXPENSE_CATEGORY, "int", "references " + CATEGORY_TABLE + " (" + CATEGORY_ID + ") "),
                new Column(EXPENSE_DATE, "text"), new Column(EXPENSE_INFORMATION, "text"));
    }

    private final void createTable(SQLiteDatabase db, String tableName, Column... columns)
    {
        String query = "create table " + tableName + "(";
        for(Column column : columns)
            query += column.name + " " + column.dataType + " " + column.constraints + ", ";

        query = query.substring(0, query.length() - 2);

        query += ");";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + EXPENSE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CATEGORY_TABLE);
    }

    public void insertInto(String tableName, AttributeValuePair... values)
    {
        ContentValues contentValues = new ContentValues();

        for(AttributeValuePair value : values)
            if(value.value instanceof String)
                contentValues.put(value.attribute, (String) value.value);
            else
                contentValues.put(value.attribute, (Integer) value.value);

        getWritableDatabase().insert(tableName, null, contentValues);
    }

    public void deleteAllEntries()
    {
        getWritableDatabase().execSQL("delete from " + CATEGORY_TABLE);
        getWritableDatabase().execSQL("delete from " + EXPENSE_TABLE);
    }
}